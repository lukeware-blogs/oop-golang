# Introdução
Fala Devs. Falaremos um pouco sobre Programação orientada a Objeto em Go. Sabem que podemos encontrar muitos conteúdos bons sobre OOP pela internet, mas vamos dar um pequena introdução sobre OOP. Convido você ficar até o final.

## O que é OOP(Object Oriented Programming)?
OOP é um paradigma(modelo) muito utilizado para desenvolvimento de software, é uma forma de se desenvolver software tipificando dados de forma personalizada, não utilizando apenas tipos primitivos como Inteiros, Boleanos, Strings e outros, nos permitindo criar novos tipos de dados como, tipo Pessoa, Usuário, Funcionário, etc. Mas não é só isso!

A OOP, trás uma séries de vantagens no desenvolvimento de software, como organização modularizada, facilidade em manter o código, Reuso do código, manutenção do código mais rápido ou encapsulamento onde você pode utilizar códigos sem precisar se preocupar com a implementação.

## O que é Abstração em OOP?
É focar em problema específicos representados por entidades do mundo real, deixa outro problemas para serem solucionados em outro momento. Para ficar mais claro, é você desenvolver pensando nas regras do negócio, regras da aplicação e não pensando em banco de dados, frameworks, tipo de comunicação ou bibliotecas. A abstração tem como princípio, isolar os objetos que iram representar o negócio da sua aplicação.

## O que é mensagem em OOP?
Os objetos que são representação do mundo real, trocam informações entre si através de mensagem, operações invocadas no objeto ou pelo objeto. Há várias formatos de mensagens, como por exemplo funções, métodos, procedures, eventos, entro outros

## O que é Objeto em OOP?
É a derivação de instâncias de uma classe.  Se você cria uma classe chamada Cliente, você poderá ter vários Objetos Cliente com os nomes, João, Maria, Diego, e por ai vai.

## O que é Atributo em OOP?
É um propriedade, um característica particular de uma classe como, Nome do cliente, Idade do cliente, documento de identificação do cliente.

## O que é Método em OOP?
É uma lógica de uma classe para atribuir um comportamento como, Cliente é maior que 18 anos? essa pergunta se tornará um método, uma forma de validação.

# Conceitos em OOP
## O que é Herança em OOP?
É um relacionamento entre classe, na qual um classe pode herda atributos, métodos de um outra classe como por exemplo, tenho um classe Animal que tem um atributo “tipo”, e um método “andar”, posso ter uma outra classe Leão e herda os métodos de Animal. Então, o Leão também tem um atributo “tipo” e um método “andar”

## O que é Encapsulamento em OOP?
uma combinação entre os atributos e os métodos de uma classe, permitindo a ocultação da complexidade do código, isso permite que, quem está usando essa classe não se preocupar com implementação dos métodos da classe.

## O que é Polimorfismo em OOP?
O polimorfismo(múltiplas formas), são objetos que podem assumir mais de um comportamento dependendo da chamada recebida. Para simplificar,  Temos o Leão que é um Animal e temos um Canguru que também é um animal. Os dois andam, mas de formas diferente. A forma que o animal Leão anda, é com as quatro patas, já a forma que o Canguru anda é com as duas patas. Exemplificamos aqui, animais com múltiplas formas de andar.

# Mão na massa
## Problema
Suponhamos que temos uma aplicação loja online. Nessa aplicação, vários pessoas podem realizar a autenticação para acessar as funcionalidades da loja. Temos os nossos clientes que usam nossa aplicação com permissão comum, onde eles podem deixar suas opinião sobre a aplicação. Temos os diretores que são funcionários com permissões administrativas. Temos os programadores que também são funcionários com permissão de desenvolvedor. Precisamos desenvolver um software que represente essa realidade, esse negócio.

Existem várias formas de se fazer essa abstração em OOP. Desenvolveremos uma delas. Lembrando que não estamos preocupados com banco de dados, serviços, frameworks, apenas com a abstração do negócio OOP.
