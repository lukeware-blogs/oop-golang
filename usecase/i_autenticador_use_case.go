package autenticador

import (
	"labsit.io/labsit-oop/entities/usuario"
)

type IAutenticadorUseCase interface {
	Autenticar(usuario usuario.IUsuario)
	Sair(usuario usuario.IUsuario)
	EstaAutenticado(usuario usuario.IUsuario) bool
}
