package autenticador

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"labsit.io/labsit-oop/entities/cliente"
	"labsit.io/labsit-oop/entities/funcionario"
	"labsit.io/labsit-oop/entities/pessoa"
	"labsit.io/labsit-oop/entities/usuario"
)

func Test_autenticar_com_cliente_comum(t *testing.T) {
	var clienteDiego usuario.IUsuario = cliente.Cliente{
		Pessoa: pessoa.Pessoa{
			Usuario: usuario.Usuario{
				Apelido:   "diego",
				Senha:     "diego123",
				Permissao: "CLIENTE_COMUM",
			},
			Nome:                 "Diego Morais",
			DocumentoIdentificao: "989.878.767-34",
		},
		OpniaoDoCliente: "Amo fazer compras nessa loja via esse aplicativo",
	}

	var autenticador IAutenticadorUseCase = new(AutenticadorUseCases)

	autenticador.Autenticar(clienteDiego)
	assert.True(t, autenticador.EstaAutenticado(clienteDiego))

}

func Test_autenticar_com_funcionario_desenvolvedor(t *testing.T) {
	var programadorJoao usuario.IUsuario = funcionario.Funcionario{
		Pessoa: pessoa.Pessoa{
			Usuario: usuario.Usuario{
				Apelido:   "joao",
				Senha:     "joao123",
				Permissao: "DESENVOLVEDOR",
			},
			Nome:                 "João Souza",
			DocumentoIdentificao: "989.888.546-34",
		},
		Cargo: "Programador front end sênior",
	}

	var autenticador IAutenticadorUseCase = new(AutenticadorUseCases)

	autenticador.Autenticar(programadorJoao)
	assert.True(t, autenticador.EstaAutenticado(programadorJoao))

}

func Test_autenticar_com_funcionario_diretor(t *testing.T) {
	var diretoraMaria usuario.IUsuario = funcionario.Funcionario{
		Pessoa: pessoa.Pessoa{
			Usuario: usuario.Usuario{
				Apelido:   "maria",
				Senha:     "maria123",
				Permissao: "DIRETORIA",
			},
			Nome:                 "Maria Alencar",
			DocumentoIdentificao: "786.456.854-23",
		},
		Cargo: "CEO",
	}

	var autenticador IAutenticadorUseCase = new(AutenticadorUseCases)

	autenticador.Autenticar(diretoraMaria)
	assert.True(t, autenticador.EstaAutenticado(diretoraMaria))

}

func Test_sair_com_cliente_comum(t *testing.T) {
	var clienteDiego usuario.IUsuario = cliente.Cliente{
		Pessoa: pessoa.Pessoa{
			Usuario: usuario.Usuario{
				Apelido:   "diego",
				Senha:     "diego123",
				Permissao: "CLIENTE_COMUM",
			},
			Nome:                 "Diego Morais",
			DocumentoIdentificao: "989.878.767-34",
		},
		OpniaoDoCliente: "Amo fazer compras nessa loja via esse aplicativo",
	}

	var autenticador IAutenticadorUseCase = new(AutenticadorUseCases)

	autenticador.Autenticar(clienteDiego)
	assert.True(t, autenticador.EstaAutenticado(clienteDiego))
	autenticador.Sair(clienteDiego)
	assert.False(t, autenticador.EstaAutenticado(clienteDiego))

}

func Test_sair_com_funcionario_desenvolvedor(t *testing.T) {
	var programadorJoao usuario.IUsuario = funcionario.Funcionario{
		Pessoa: pessoa.Pessoa{
			Usuario: usuario.Usuario{
				Apelido:   "joao",
				Senha:     "joao123",
				Permissao: "DESENVOLVEDOR",
			},
			Nome:                 "João Souza",
			DocumentoIdentificao: "989.888.546-34",
		},
		Cargo: "Programador front end sênior",
	}

	var autenticador IAutenticadorUseCase = new(AutenticadorUseCases)

	autenticador.Autenticar(programadorJoao)
	assert.True(t, autenticador.EstaAutenticado(programadorJoao))
	autenticador.Sair(programadorJoao)
	assert.False(t, autenticador.EstaAutenticado(programadorJoao))

}

func Test_sair_com_funcionario_diretor(t *testing.T) {
	var diretoraMaria usuario.IUsuario = funcionario.Funcionario{
		Pessoa: pessoa.Pessoa{
			Usuario: usuario.Usuario{
				Apelido:   "maria",
				Senha:     "maria123",
				Permissao: "DIRETORIA",
			},
			Nome:                 "Maria Alencar",
			DocumentoIdentificao: "786.456.854-23",
		},
		Cargo: "CEO",
	}

	var autenticador IAutenticadorUseCase = new(AutenticadorUseCases)

	autenticador.Autenticar(diretoraMaria)
	assert.True(t, autenticador.EstaAutenticado(diretoraMaria))
	autenticador.Sair(diretoraMaria)
	assert.False(t, autenticador.EstaAutenticado(diretoraMaria))

}

func Test_autenticar_com_funcionario_diretor_sendo_usuario(t *testing.T) {
	var diretoraMaria usuario.IUsuario = funcionario.Funcionario{
		Pessoa: pessoa.Pessoa{
			Usuario: usuario.Usuario{
				Apelido:   "maria",
				Senha:     "maria123",
				Permissao: "DIRETORIA",
			},
			Nome:                 "Maria Alencar",
			DocumentoIdentificao: "786.456.854-23",
		},
		Cargo: "CEO",
	}

	var diretoraMariaFunc funcionario.IFuncionario = funcionario.Funcionario{
		Pessoa: pessoa.Pessoa{
			Usuario: usuario.Usuario{
				Apelido:   "maria",
				Senha:     "maria123",
				Permissao: "DIRETORIA",
			},
			Nome:                 "Maria Alencar",
			DocumentoIdentificao: "786.456.854-23",
		},
		Cargo: "CEO",
	}

	var autenticador IAutenticadorUseCase = new(AutenticadorUseCases)

	autenticador.Autenticar(diretoraMariaFunc)
	assert.True(t, autenticador.EstaAutenticado(diretoraMaria))

	autenticador.Autenticar(diretoraMariaFunc)
	assert.True(t, autenticador.EstaAutenticado(diretoraMaria))

}
