package autenticador

import (
	"fmt"

	"labsit.io/labsit-oop/entities/usuario"
)

type AutenticadorUseCases struct {
}

var usuarios map[usuario.IUsuario]bool = make(map[usuario.IUsuario]bool)

func (a AutenticadorUseCases) Autenticar(usuario usuario.IUsuario) {
	fmt.Printf("Usuário %s com permissão %s, auntenticado com suscesso\n", usuario.GetApelido(), usuario.GetPermissao())
	usuarios[usuario] = true
}

func (a AutenticadorUseCases) Sair(usuario usuario.IUsuario) {
	fmt.Printf("Usuário %s com permissão %s, saiu da aplicação\n", usuario.GetApelido(), usuario.GetPermissao())
	usuarios[usuario] = false
}

func (a AutenticadorUseCases) EstaAutenticado(usuario usuario.IUsuario) bool {
	return usuarios[usuario]
}
