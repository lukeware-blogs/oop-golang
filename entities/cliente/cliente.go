package cliente

import "labsit.io/labsit-oop/entities/pessoa"

type Cliente struct {
	pessoa.Pessoa
	OpniaoDoCliente string
}

func (c Cliente) GetOpiniaoDoCliente() string {
	return c.OpniaoDoCliente
}