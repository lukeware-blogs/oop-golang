package cliente

import "labsit.io/labsit-oop/entities/pessoa"

type ICliente interface {
	pessoa.IPessoa
	GetOpiniaoDoCliente() string
}
