package usuario

type IUsuario interface {
	GetApelido() string
	GetSenha() string
	GetPermissao() string
}
