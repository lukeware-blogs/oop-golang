package usuario

type Usuario struct {
	Apelido   string
	Senha     string
	Permissao string
}

func (u Usuario) GetApelido() string {
	return u.Apelido
}

func (u Usuario) GetSenha() string {
	return u.Senha
}

func (u Usuario) GetPermissao() string {
	return u.Permissao
}
