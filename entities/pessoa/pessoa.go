package pessoa

import "labsit.io/labsit-oop/entities/usuario"

type Pessoa struct {
	usuario.Usuario
	Nome                 string
	DocumentoIdentificao string
}

func (p Pessoa) GetNome() string {
	return p.Nome
}

func (p Pessoa) GetDocumentoIdentificao() string {
	return p.DocumentoIdentificao
}

