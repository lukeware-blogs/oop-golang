package pessoa

import "labsit.io/labsit-oop/entities/usuario"

type IPessoa interface {
	usuario.IUsuario
	GetNome() string
	GetDocumentoIdentificao() string
}
