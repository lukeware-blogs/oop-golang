package funcionario

import (
	"labsit.io/labsit-oop/entities/pessoa"
)

type IFuncionario interface {
	pessoa.IPessoa
	GetCargo() string
}
