package funcionario

import "labsit.io/labsit-oop/entities/pessoa"

type Funcionario struct {
	pessoa.Pessoa
	Cargo string
}

func (f Funcionario) GetCargo() string {
	return f.Cargo
}

